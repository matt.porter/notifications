#!/usr/bin/env python2.6
############################################################################################
# APNS Request Dispatcher (multiapp support)
# designed and coded by Cal Leeming (Codebrush/PixelMags 2009)
############################################################################################

import sys
import time

#from APNSWrapper import *
try:
    import MySQLdb
    import MySQLdb.cursors
except ImportError:
    import pymysql as MySQLdb
    import pymysql.cursors
import random
import socket
import base64
import ssl

import datetime
import struct
import tempfile
import os
import smtplib
from email.Header import Header

import apns_formats

smtpconn = None
MAX_NS_RETRIES = 1

def debugger(msg, report=False, raiseException=False):
    ts = time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime())
    print "[%s] %s" % (ts, msg)

    try:
        raise Exception, msg
    
    except:
        if report:
            ReportBug()
            
        if raiseException:
            raise

def verify_cert(cert_file):
    """Verify that the certificate hasn't expired yet, returns True on success"""
    try:
        from OpenSSL.SSL import Context, TLSv1_METHOD, VERIFY_PEER, VERIFY_FAIL_IF_NO_PEER_CERT, OP_NO_SSLv2
        from OpenSSL.crypto import load_certificate, FILETYPE_PEM
    except ImportError:
        # we don't have OpenSSL libs so we just presume it's OK
        return True

    cert_data = open(cert_file).read()
    cert = load_certificate(FILETYPE_PEM, cert_data)
    assert cert is not None

    notafter = cert.get('notAfter')
    if notafter and time.time() > ssl.cert_time_to_seconds(notafter):
        return False
    return True


def join_dicts(t1, t2, key):
    """join the two tables of dictionaries based on a common field
    """
    t3 = []
    for d1 in t1:
        d3 = {}
        d3.update(d1)
        joins = [d for d in t2 if d[key] == d1[key]]
        if joins:
            d3.update(joins[0])
        t3.append(d3)

    return t3


def batch(iter, batch_size):
    """Return the original list grouped in batches of max length batch_size
    e.g
    >>> batch(range(17), 3)
    [[0, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11], [12, 13, 14], [15, 16]]
    """
    return [iter[n : n + batch_size] for n in range(0, len(iter), batch_size)]


class APNSFeedbackWrapper(object):
    """
    This object wrap Apple Push Notification Feedback Service tuples.
    Object support for iterations and may work with routine cycles like for.
    """
    apnsHost = 'feedback.push.apple.com'
    apnsSandboxHost = 'feedback.sandbox.push.apple.com'
    apnsPort = 2196
    feedbacks = None
    connection = None
    testingParser = False

    blockSize = 1024 # default size of SSL reply block is 1Kb
    feedbackHeaderSize = 6

    enlargeRecursionLimit = lambda self: sys.setrecursionlimit(sys.getrecursionlimit() + 100)

    _currentTuple = 0
    _tuplesCount = 0

    def __init__(self, connection):
        self.debug_ssl = False
        self.force_ssl_command = False
        self.connection = connection
        self.feedbacks = []
        self._currentTuple = 0
        self._tuplesCount = 0

    def __iter__(self):
        return self

    def next(self):
        if self._currentTuple >= self._tuplesCount:
            raise StopIteration

        obj = self.feedbacks[self._currentTuple]
        self._currentTuple += 1
        return obj

    def _parse_reply(self, reply):
        flag = True
        offset = 0
        while flag:
            try:
                feedbackTime, tokenLength = struct.unpack_from('!lh', reply, offset)
                deviceToken = struct.unpack_from('%ds' % tokenLength, reply, offset + 6)[0]
                offset += 6 + len(deviceToken)

                self._append(feedbackTime, deviceToken)
            except:
                flag = False

    def tuples(self):
        """
        This method return a list with all received deviceTokens:
        ( datetime, deviceToken )
        """
        return self.feedbacks

    def _append(self, fTime, token):
        self.feedbacks.append((datetime.datetime.fromtimestamp(fTime), token))
        self._tuplesCount = len(self.feedbacks)

    def _parseHeader(self, Buff):
        """
        Parse header of Feedback Service tuple.
        Format of Buff is |xxxx|yy|zzzzzzzz|
            where:
                x is time_t (UNIXTIME, long, 4 bytes)
                y is length of z (two bytes)
                z is device token
        """
        try:
            feedbackTime, tokenLength = struct.unpack_from('!lh', Buff, 0)
            if Buff >= self.feedbackHeaderSize + tokenLength:
                recoursiveInvoke = lambda: self._parseTuple(feedbackTime, tokenLength, Buff[self.feedbackHeaderSize:])

                # enlarge recursion limit if it is exceeded
                try:
                    return recoursiveInvoke()
                except RuntimeError:
                    self.enlargeRecursionLimit()
                    return recoursiveInvoke()
            else:
                return Buff
        except:
            return Buff

    def _parseTuple(self, tTime, tLen, Buff):
        """
        Get body by length tLen of current Feedback Service tuple.
        If body length is equal to tLen than append new
        tuple item and recoursive parse next item.

        """
        try:
            token = struct.unpack_from('!%ds' % tLen, Buff, 0)[0]            
            #device_token = struct.unpack_from('!%ds' % len(token) / 2, token, 2)[0]
            self._append(tTime, token) 
        except:
            pass

        recurrenceInvoke = lambda: self._parseHeader(Buff[tLen:])
        # enlarge recursion limit if it is exceeded
        try:
            return recurrenceInvoke()
        except RuntimeError:
            self.enlargeRecursionLimit()
            return recurrenceInvoke()

    def _testFeedbackFile(self):
        fh = open('feedbackSampleTuple.dat', 'r')
        return fh

    def receive(self):
        """
        Receive Feedback tuples from APNS:
            1) make connection to APNS server and receive
            2) unpack feedback tuples to arrays
        """

        apnsConnection = self.connection

        tRest = None
        blockSize = self.blockSize

        # replace connectionContext to similar I/O function but work
        # with binary Feedback Service sample file
        if self.testingParser:
            connectionContext = self._testFeedbackFile()

        replyBlock = apnsConnection.read(blockSize)

        while replyBlock:
            if tRest and len(tRest) > 0:
                # merge previous rest of replyBlock and new
                replyBlock = struct.pack('!%ds%ds' % (len(tRest), len(replyBlock)), tRest, replyBlock)
            tRest = self._parseHeader(replyBlock)
            replyBlock = apnsConnection.read(blockSize)

        # close sample binary file
        if self.testingParser:
            connectionContext.close()

        return True


class APNSDispatcher:
    db = None
    ext_db = None
    db4 = None

    def __init__(self, dbconf_main, dbconf_ext, dbconf_db4, conf, priorities):
        self.dbconf_main = dbconf_main
        self.dbconf_ext = dbconf_ext
        self.conf = conf
        self.dbconf_db4 = dbconf_db4
        self.priorities = priorities
        
    def makeWork(self):
        self.random1 = random.getrandbits(31)
        self.random2 = random.getrandbits(31)

    def get_magazine_app(self, app_id):
        """Return a dictionary containing a magazine row"""
        cursor = self.db.cursor(MySQLdb.cursors.DictCursor)
        sql = """
            SELECT
                ma.apns_cert, ma.apns_key, ma.pem_passphrase, ma.magazine_id
            FROM
                magazine_apps ma
            WHERE
                ma.id = "%s"
            LIMIT 1
        """ % (app_id,)
        cursor.execute(sql)
        app = cursor.fetchone()
        if not app:
            return None

        # check if the magazine has a configured apns certificate and key
        if app['apns_cert'] is None or app['apns_key'] is None:
            self.debugger("Magazine app does not have a configured APNS Cert/Key (App ID: %s)"%(app_id,), report=True)
            return None
        return app

    def create_magazine_connection(self, app_id):
        """Return a SSLSocket which can send APNS messages for the specified magazine"""

        app = self.get_magazine_app(app_id)
        if app is None:
            return None

        # Write APNS certificates to disk and open connection
        # Certificates must be removed afterwards
        keyloc = None
        sc = None
        sc2 = None
        try:
            certtup = tempfile.mkstemp(str(app_id), self.conf.ENVIRONMENT) #'keycache/cert-apns-%s-%s.pem' % (ENVIRONMENT, magazine_id)
            certloc = certtup[1]
            f = open(certloc, 'wb')
            f.write(app['apns_cert'])
            f.close()


            # Verify cert function not in use?
            #
            #if not verify_cert(certloc):
            #    return None


            # cache the key location
            keytup = tempfile.mkstemp(str(app_id), self.conf.ENVIRONMENT) #'keycache/key-apns-%s-%s.pem' % (ENVIRONMENT, magazine_id)
            keyloc = keytup[1]
            f = open(keyloc, 'wb')
            f.write(app['apns_key'])
            f.close()

            self.debugger("Opening APNS send socket to %s" % self.conf.SERVER)
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sc = ssl.wrap_socket(s, ssl_version=ssl.PROTOCOL_SSLv3, certfile=certloc, keyfile=keyloc)
            sc.connect((self.conf.SERVER, 2195))

            self.debugger("Opening APNS feedback socket to %s" % self.conf.FEEDBACK)
            s2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s2.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sc2 = ssl.wrap_socket(s2, ssl_version=ssl.PROTOCOL_SSLv3, certfile=certloc, keyfile=keyloc)
            sc2.connect((self.conf.FEEDBACK, 2196))
        finally:
            # Once they are connected we don't need to keep the certificate & key around.
            os.remove(certloc)
            if keyloc and os.path.exists(keyloc):
                os.remove(keyloc)

        # Extract feedback information
        magazine_id = app['magazine_id']
        if sc2:
            try:
                APNSFeedback(self.db, sc2, magazine_id, app_id)
            finally:
                sc2.close()

        return sc

    def clearUnworked(self, broken_id_list = None):
        self.create_database_connection()
        cursor = self.ext_db.cursor(MySQLdb.cursors.DictCursor)

        broken_id_str = ""
        if broken_id_list:
            broken_id_str = 'AND nq.id IN(' + ','.join(map(str, broken_id_list)) + ')'

        cursor.execute("""
        UPDATE notification_queue nq SET nq.is_working = 0, nq.is_deleted = 0, nq.lastModified = NOW()
         WHERE nq.is_working = %d
           AND nq.is_deleted = %d
           %s
        """ % (self.random1, self.random2, broken_id_str))
        self.ext_db.commit()

    def finishWork(self, finished_id_list = None):
        self.create_database_connection()
        cursor = self.ext_db.cursor(MySQLdb.cursors.DictCursor)

        finish_str = ""
        if finished_id_list:
            finish_str = 'AND nq.id IN(' + ','.join(map(str, finished_id_list)) + ')'

        # pick which queue entries we're going to work on
        cursor.execute("""
            UPDATE notification_queue nq
               SET is_working = 0
                  ,is_deleted = 1
                  ,lastModified = NOW()
             WHERE (nq.after_date <= NOW() OR nq.after_date IS NULL)
               AND nq.is_working = %d
               AND nq.is_deleted = %d
               AND nq.after_date >= (NOW() - INTERVAL 1 WEEK)
               %s
        """ % (self.random1, self.random2, finish_str))
        self.ext_db.commit()

    # Used to extract the elements from the queue
    def getWork(self):
        self.create_database_connection()
        x = {}
        try:
            self.debugger("Querying for all APNS Queue entries")
            cursor = self.db.cursor(MySQLdb.cursors.DictCursor)
            cursor_ext = self.ext_db.cursor(MySQLdb.cursors.DictCursor)
            cursor_db4 = self.db4.cursor(MySQLdb.cursors.DictCursor)

            # pick which queue entries we're going to work on
            # Operates only on extended DB

            # priority list added so that we can run a second notification sender which picks up
            # priority issues or jobs. Set up for the Olympic App daily notification
            issues_priority_filter = ''
            jobs_priority_filter = ''
            if self.priorities:
                issues_list = self.priorities.get('issue_id',0) or []
                jobs_list = self.priorities.get('dispatch_id',0) or []
                if issues_list:
                    issues_priority_filter = 'AND nq.issue_id in (%s)' % ','.join(map(str, issues_list))
                if jobs_list:
                    jobs_priority_filter = 'AND nq.dispatch_id in (%s)' % ','.join(map(str, jobs_list))
                    
            # exclusion list added since Hearst decided to cancel around 8 million notifications
            exclude_list = get_cancelled_jobs(self.db4) or ''
            if exclude_list:
               exclude_list = "AND nq.dispatch_id NOT IN (%s)" % ','.join(map(str, exclude_list))

            debugger("Excluding the following cancelled jobs: " + exclude_list)
            self.makeWork()
            cursor_ext.execute("""
                UPDATE notification_queue nq
                   SET is_working = %(r1)d
                      ,is_deleted = %(r2)d
                WHERE (nq.after_date <= NOW() OR nq.after_date IS NULL)
                   %(exclude_list)s
                   %(priority_issues)s
                   %(priority_jobs)s
                   AND nq.is_deleted = 0
                   AND nq.is_working = 0
                   AND nq.after_date >= (NOW() - INTERVAL 1 WEEK)
                LIMIT 1000
            """ % {'r1' : self.random1, 'r2' : self.random2, 'exclude_list' : exclude_list, 'priority_issues' : issues_priority_filter, 'priority_jobs' : jobs_priority_filter})
            self.ext_db.commit()

            debugger("Getting work: WHERE is_working = %d AND is_deleted = %d)" % (self.random1, self.random2))

            # and pull them back in now that they're locked
            # First get from the notification queue (extended), then main.
            cursor_ext.execute("""
                SELECT nq.id, 
                       nq.id AS apns_message_id,
                       nq.message AS apns_message,
                       nq.issue_id,
                       nq.magazine_users_id,
                       nq.email_to,
                       nq.email_from,
                       nq.email_subject,
                       nq.email_body,
                       nq.notify_newsstand,
                       nq.newsstand_retries
                 FROM notification_queue nq
                 WHERE nq.is_working = %d
                   AND nq.is_deleted = %d
            """ %(self.random1,self.random2))

            notifications = cursor_ext.fetchall()

            if not len(notifications):
                self.debugger("No queued notifications found")
                return

            mag_user_ids = [str(row['magazine_users_id']) for row in notifications]
            sql_valuestr = ','.join(mag_user_ids)

            sql = """
                SELECT ud.user_id, ud.device_id,
                       mu.id as magazine_users_id,
                       mu.magazine_ID AS magazine_id,
                       mu.notificationToken AS notification_token,
                       mu.background_download_available,
                       mu.allow_apns,
                       mu.allow_email,
                       app.bundle_name,
                       app.ID as app_id,
                       u.suppressMarketingEmails
                FROM magazine_users mu
                    LEFT JOIN userdevice ud ON (mu.userdevice_id = ud.ID)
                    LEFT JOIN magazine_apps app ON (mu.magazine_apps_id = app.ID)
                    LEFT JOIN users u ON (u.id = ud.user_id)
                WHERE mu.ID IN (%s)
            """ % (sql_valuestr, )
            cursor_db4.execute(sql)

            mag_users = cursor_db4.fetchall()
            if not len(mag_users):
                self.debugger("No queued notifications found with users")
                return

            result_set = join_dicts(notifications, mag_users, 'magazine_users_id')

            issue_ids = list(set([str(row['issue_id']) for row in notifications if row['issue_id']]))
            if issue_ids:
                sql_valuestr = ','.join(issue_ids)
                cursor_db4.execute("""
                    SELECT issue.ID as issue_id,
                           issue.isPublished,
                           issue.remove_from_sale,
                           issue.issueDate
                    FROM issue
                    WHERE issue.ID IN (%s)
                """ % (sql_valuestr))
                issues = cursor_db4.fetchall()

                result_set = join_dicts(result_set, issues, 'issue_id')

            cursor.close()

            for result in result_set:
                if not result['app_id']:
                    # need an app - else no certificate
                    continue
                if not x.has_key(result['app_id']):
                    x[result['app_id']] = []
                x[result['app_id']].append(result)

            return x

        except MySQLdb.Error, e:
            self.debugger("Database error: %s"%e, report=True, raiseException=True)

    def create_database_connection(self):
        if not self.db:
            conf = self.dbconf_main
            self.db = MySQLdb.connect(host=conf.DB_HOST, user=conf.DB_USER, passwd=conf.DB_PASSWD, db=conf.DB_NAME, use_unicode=True, charset='utf8')
            assert self.db is not None

        if not self.ext_db:
            conf = self.dbconf_ext
            self.ext_db = MySQLdb.connect(host=conf.DB_HOST, user=conf.DB_USER, passwd=conf.DB_PASSWD, db=conf.DB_NAME, use_unicode=True, charset='utf8')
            assert self.ext_db is not None

        if not self.db4:
            conf = self.dbconf_db4
            self.db4 = MySQLdb.connect(host=conf.DB_HOST, user=conf.DB_USER, passwd=conf.DB_PASSWD, db=conf.DB_NAME, use_unicode=True, charset='utf8')
            assert self.db4 is not None
            
        return self.db

    def close_database_connection(self):
        if self.db:
            self.db.close()
            self.db = None


    def debugger(self, msg, report=False, raiseException=False):
        ts = time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime())
        print "[%s] %s" % (ts, msg)
    
        try:
            raise Exception, msg
        
        except:
            if report:
                ReportBug()
                
            if raiseException:
                raise

def split_entries(entries):
    """separate the entries into newsstand, apns, and email.
    - Email / apns are sent at the same time as the first newsstand notification.
    """
    apns, email, newsstand, not_sending = [], [], [], []

    date_limit = datetime.datetime.now() - datetime.timedelta(days=10)
    for entry in entries:
        if 'isPublished' in entry and entry['isPublished'] == 'No' or 'remove_from_sale' in entry and entry['remove_from_sale'] == 'Yes':
            debugger("not sending id:%s -- issue %s unpublished or removed from sale" % (entry['id'], entry['issue_id']))
            not_sending.append(entry)
        elif 'issueDate' in entry and entry['issueDate'] and entry['issueDate'] > datetime.datetime.now():
            debugger("not sending id:%s -- issue %s publish date has yet to have passed" % (entry['id'], entry['issue_id']))
            not_sending.append(entry)
        elif 'issueDate' in entry and entry['issueDate'] and entry['issueDate'] < date_limit:
            debugger("not sending id:%s -- issue %s too old" % (entry['id'], entry['issue_id']))
            not_sending.append(entry)
        else:
            sending_any = False
            if entry['apns_message'] and entry["background_download_available"] and entry['notify_newsstand'] > 0 and entry['newsstand_retries'] < MAX_NS_RETRIES and entry['issue_id'] and entry["notification_token"]:
                newsstand.append(entry)
                sending_any = True
            if entry['email_to'] and not entry['email_to'].endswith('@temp.pixel-mags.com') and \
               entry["allow_email"] and not entry["suppressMarketingEmails"]:
                email.append(entry)
                sending_any = True
            if entry["apns_message"] and entry["notification_token"] and entry["allow_apns"] and entry["newsstand_retries"] == 0:
                apns.append(entry)
                sending_any = True
            if not sending_any:
                debugger("Nothing to send for id:%s" % (entry['id']))
                not_sending.append(entry)
    return (apns, email, newsstand, not_sending)


def send_all_emails(entries):
    sent_row_ids = []
    failed_row_ids = []

    for entry in entries:
        if entry['email_to'] and not entry['email_to'].endswith('@temp.pixel-mags.com'):
            debugger("Sending email: id %(id)s to %(email_to)s" % entry)
            try:
                SendEmail(
                    entry['email_from'],
                    entry['email_to'],
                    entry['email_subject'].encode('utf-8'),
                    entry['email_body'].encode('utf-8')
                )
            except:
                debugger("Email failed to %(email_to)s. Notification ID %(id)s" % entry)
            sent_row_ids.append(entry['id'])

    return sent_row_ids, failed_row_ids


def send_all_apns(entries, dispatcher, newsstand = False):
    a = dispatcher
    max_retries = 3
    flag_expired = False # if the certificate comes back as expired, give up on this app
    flag_reconnect = True
    no_connection = False
    sent_row_ids = []
    failed_row_ids = []
    magazine_connection = None

    for entry in entries:
        apns_sent = False
        if no_connection:
            break
        for retry_num in range(max_retries):
            if apns_sent or flag_expired or no_connection:
                break
            if flag_reconnect:
                try:
                    magazine_connection = a.create_magazine_connection(entry["app_id"])
                    time.sleep(0.2)
                except ssl.SSLError, e:
                    flag_expired = e.strerror.endswith("routines:SSL3_READ_BYTES:sslv3 alert certificate expired")
                    ReportBug()
                except socket.error, e:
                    # These will be cleared and worked on again at some point
                    pass
                if flag_expired:
                    #Give up for this app.
                    break
                if magazine_connection is None:
                    no_connection = True
                    break
                flag_expired = False
                flag_reconnect = False

            # No point in this one if there's no message
            if not entry['apns_message']:
                continue

            try:
                msg = apns_formats.convert_simple_message_to_enhanced(entry['apns_message_id'], entry['apns_message'], newsstand=newsstand)
                debugger("Push notification: ID %(id)s, mu.ID %(magazine_users_id)s" % entry)
                magazine_connection.send(msg)
                sent_row_ids.append(entry["id"])
                apns_sent = True
            except Exception, e:
                # Broken pipes? - reconnect
                print str(e), "Attempt", retry_num
                flag_reconnect = True
                continue

            # Read a response from Apple to get any issues
            response = None
            timeout = magazine_connection.gettimeout()
            try:
                magazine_connection.settimeout(0) # no response if OK, so need to timeout
                response = magazine_connection.recv(6) #Response is 6 bytes.
                if len(response) == 6:
                    debugger("Read response ( %s ): handle it" % (response, ))
                    handle_response(response, {"message" : msg})
            except ssl.SSLError, e:
                if not str(e).find("timed out"):
                    debugger("Unexpected SSLError reading response. ID %d\nException %s" % (entry['apns_message_id'], e))
            except Exception, e:
                debugger("Unexpected exception when reading response. ID %d\nException %s" % (entry['apns_message_id'], e))
            finally:
                #Connection will always be closed after receiving response.
                magazine_connection.settimeout(timeout)
                if response or not magazine_connection:
                    flag_reconnect = True

    return sent_row_ids, failed_row_ids


def issue_has_been_downloaded(issue_id, device_id, db):
    """Checks for any downloads of this issue on this device
    Return True if we find a download report for this device/issue, else False.
    """
    cursor = db.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute("""  SELECT ud.id userdevice_id, dr.id AS download_report_id from userdevice ud
                        LEFT JOIN download_reports dr
                        ON (dr.device_id = ud.id)
                        WHERE ud.device_id = %s
                        AND dr.issue_id = %s""", (device_id, issue_id))
    rows = cursor.fetchall()
    if not rows:
        return False

    for row in rows:
        if row['download_report_id']:
            return True

    return False


def remove_downloaded(entries, db, ext_db):
    """Remove any entries which have a download report from the app.
    Download reports are in the main database, but notification queue has to be updated

    Note: the download_reports table stores a userdevice_id as 'device_id'
          We check against all userdevice_ids for the device_id we are notifying, as downloads are per-device.
    """
    pruned_entries = []
    finished_ids = []

    for entry in entries:
        if issue_has_been_downloaded(entry['issue_id'], entry['device_id'], db):
            finished_ids.append(entry["id"])
        else:
            pruned_entries.append(entry)

    if finished_ids:
        ext_cursor = ext_db.cursor(MySQLdb.cursors.DictCursor)
        sql = """UPDATE notification_queue
                 SET is_deleted = 1, is_working = 0
                 WHERE id in (%s)""" % (",".join([str(id) for id in finished_ids]))
        ext_cursor.execute(sql)
        ext_db.commit()

    return pruned_entries


def update_newsstand_rows(ids, ext_db):
    """ For each row id, update newsstand_retries and set is_deleted and is_working back to zero
    after date needs to be updated to 24 hours from now.
    """
    if not ids:
        return
    cursor = ext_db.cursor(MySQLdb.cursors.DictCursor)
    sql = """update notification_queue nq set
            nq.is_deleted = 0, nq.is_working = 0,
            nq.after_date = (NOW() + INTERVAL 1 DAY),
            nq.newsstand_retries = (nq.newsstand_retries + 1)
    where nq.id IN (%s)""" % (",".join([str(id) for id in ids]))
    cursor.execute(sql)

    sql = """update notification_queue nq set
             nq.is_deleted = 1
             where nq.id IN (%s)
             AND nq.newsstand_retries >= %s""" % (",".join([str(id) for id in ids]), MAX_NS_RETRIES)
    cursor.execute(sql)

    ext_db.commit()
    
    
def send_all_newsstand(entries, dispatcher):
    """ Send newsstand notifications
    - check download_report before sending.
    - increment retry count for each entry
    - if retry count > num retries, send standard apns/email.
    - after sending, set after_date to after_date + 24 hours
    """
    # Check if the user has downloaded these
    entries = remove_downloaded(entries, dispatcher.db4, dispatcher.ext_db)
    successful, failed = send_all_apns(entries, dispatcher, newsstand=True)
    update_newsstand_rows(successful + failed, dispatcher.ext_db)
    return successful, failed


def update_notification_job(issue_id, dispatcher):
    """ For each issue id, get a count of the issue in notification_job and set delayed_notification_job 
    status to either sending or sent on if the count is greater than zero
    """
    if issue_id:
        cursor = dispatcher.ext_db.cursor(MySQLdb.cursors.DictCursor)
        sql = """SELECT count(nq.issue_id) as leftovers
                FROM notification_queue nq
                where
                NOT nq.is_deleted = 1 AND
                nq.issue_id = %s""" % issue_id
        cursor.execute(sql)
        results = cursor.fetchone()

        if results['leftovers'] > 0:
            sql = """UPDATE delayed_notification_jobs dnj SET
                dnj.status = "sending"
                where dnj.issue_id = %s
                and dnj.status != "queuing" """ % issue_id
        else:
            sql = """UPDATE delayed_notification_jobs dnj SET
                dnj.status = "sent",
                dnj.apns_sent = dnj.apns_queued,
                dnj.email_sent = dnj.email_queued
                where dnj.issue_id = %s
                and dnj.status != "queuing" """ % issue_id

        cursor = dispatcher.db.cursor(MySQLdb.cursors.DictCursor)

        cursor.execute(sql)
        dispatcher.db.commit()
   

def push_back_notification_dates(entries, dispatcher):
    """ takes a list of notification that were created and then had there issueDate
    pushed back and therefore shouldn't be sent until the correct date
    """   
    if not entries:
        return 
    new_date = entries[0]['issueDate']
    cursor = dispatcher.ext_db.cursor(MySQLdb.cursors.DictCursor)
    sql = """update notification_queue nq set
    nq.after_date = '%s'
    where nq.id IN (%s)""" % (new_date, ",".join(str(e['id']) for e in entries))

    cursor.execute(sql)
    dispatcher.ext_db.commit()


def get_cancelled_jobs(db_ro):
    """A job can be stopped by setting 'send_apns=0 and send_email=0' on the notification queue
    This function finds the jobs which could still be sending, and returns a list of dispatch ids
    which should be excluded
    """
    sql = """
            SELECT id from delayed_notification_jobs
            WHERE (after_date <= NOW() OR after_date IS NULL)
            AND after_date >= (NOW() - INTERVAL 1 WEEK)
            AND `status` in ('queuing', 'queued', 'sending')
            AND send_apns = 0
            AND send_email = 0
            """
    cursor = db_ro.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute(sql)
    rows = cursor.fetchall()
    ids = [row['id'] for row in rows]

    return ids


def Run(conf_main, conf_ext, conf_db4, conf_env, priorities = None):
    """
    fetch and send entries from the queue table
    return: False if no entries, True otherwise.
    """
    try:
        dispatcher = APNSDispatcher(conf_main, conf_ext, conf_db4, conf_env, priorities)
        entries = dispatcher.getWork()
        if not entries:
            return False

        for app_id in entries:
            issue_id = entries[app_id][0]['issue_id']
            queue = entries[app_id]
            apns, emails, newsstand, not_sending = split_entries(queue)
            to_finish = [e['id'] for e in not_sending]
            to_push_back = [e for e in not_sending if 'issueDate' in e and e['issueDate'] and e['issueDate'] > datetime.datetime.now()]
            debugger("Not sending %s entries for app_id %s issue %s" % (len(not_sending), app_id, issue_id))

            if emails:
                debugger("Sending emails for appid %s" % (app_id,))
                successful_emails, failed_emails = send_all_emails(emails)
                debugger("Sent %d emails for appid %s. %s failed" % (len(successful_emails), app_id, len(failed_emails), ))
                to_finish.extend(successful_emails)

            if apns:
                debugger("Sending apns for appid %s" % (app_id,))
                successful_apns, failed_apns = send_all_apns(apns, dispatcher)
                debugger("Sent %d apns for appid %s. %s failed" % (len(successful_apns), app_id, len(failed_apns), ))

                newsstand_ids = [entry["id"] for entry in newsstand]
                finishing_apns = [id for id in successful_apns if id not in newsstand_ids]
                to_finish.extend(finishing_apns)

            if newsstand:
                debugger("Sending newsstand for appid %s" % (app_id,))
                successful_newsstand, failed_newsstand = send_all_newsstand(newsstand, dispatcher)
                debugger("Sent %d newsstand for appid %s. %s newsstand" % (len(successful_newsstand), app_id, len(failed_newsstand), ))

            push_back_notification_dates(to_push_back, dispatcher)
            dispatcher.finishWork(list(set(to_finish)))
            update_notification_job(issue_id, dispatcher)
        return True

    except Exception, e:
        ReportBug()
        raise


def send_until_done(conf_main, conf_ext, conf_db4, conf_env):
    running = True
    while running:
        running = Run(conf_main, conf_ext, conf_db4, conf_env)
        # as a safeguard for high DB load, sleep for 5 before calling again.
        # this is especially important if there are a large batch which are not being sent
        time.sleep(5)
###############################################################################

#
# Handle Feedback from APNS
#
# A notification implies that the application is no-longer existant on the device
# Hence we need to remove from database & not send any more notifications
#
def APNSFeedback(db, feedback_socket, magazine_ID, app_id ):
    feedback = APNSFeedbackWrapper(feedback_socket)
    feedback.receive()
    if not feedback.feedbacks:
        return

    MAX_BATCH = 250

    num_feedbacks = len(feedback.feedbacks)
    debugger("Processing %d APNS feedback entries for magazine %s, app %s"%(num_feedbacks, magazine_ID, app_id))

    # magazine_users stores notification_token base64 encoded
    all_tokens = ["'%s'" % (base64.b64encode(msg[1])) for msg in feedback.feedbacks if msg is not None and len(msg) == 2]
    for tokens in batch(all_tokens, MAX_BATCH):
        #debugger(tokens[0])
        tokenstr = (','.join(tokens))
        cursor = db.cursor(MySQLdb.cursors.DictCursor)
        sql = """SELECT ID, magazine_ID, userdevice_ID, notificationToken FROM magazine_users WHERE magazine_ID = %s AND magazine_apps_id = %s AND notificationToken IN (%s)""" % (magazine_ID, app_id, tokenstr)
        cursor.execute(sql)
        magazine_users = cursor.fetchall()
        #debugger(sql)
        if not magazine_users:
            debugger("No users found for %s tokens" % (len(tokens),))
            return

        mu_IDs = [str(mu['ID']) for mu in magazine_users]
        cursor = db.cursor()
        cursor.execute("""          UPDATE magazine_users SET notificationToken = NULL         WHERE ID in (%s)    """ % (','.join(mu_IDs),))
        debugger("APNS Feedback for magazine_users %s successfully processed (magazine: %s, app: %s)" % (', '.join(mu_IDs), magazine_ID, app_id))

        # Log the feedback
        cursor = db.cursor()
        insert_values = [ (mu["notificationToken"], magazine_ID, mu["userdevice_ID"]) for mu in magazine_users]
        cursor.executemany("INSERT INTO apns_feedback (token, magazine_ID, userdevice_ID) VALUES (%s, %s, %s)", insert_values)

        db.commit()
        

###############################################################################    
from reportbug import ReportBug

###############################################################################

def SendEmail(email_from, email_to, email_subject, email_body):
    global smtpconn
    if not smtpconn:
        smtpconn = smtplib.SMTP('localhost')
    s = smtpconn
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText

    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg.set_charset('utf-8')
    msg['Subject'] = Header(email_subject, 'utf-8')
    msg['From'] = email_from
    msg['To'] = email_to

    # Create the body of the message (a plain-text and an HTML version).
    text = "You must view this email as HTML to view its contents."
    html = email_body

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')
    del part2['Content-Transfer-Encoding']
    part2.set_charset('utf-8')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    # Send the message via local SMTP server.
    # Using a persistent SMTP connection now
    #s = smtplib.SMTP('localhost')

    # sendmail function takes 3 arguments: sender's address, recipient's address
    # and message to send - here it is sent as one string.
    try:
        s.sendmail(email_from, email_to, msg.as_string())
        
    except smtplib.SMTPRecipientsRefused:
        pass

    #s.quit()


def handle_response(response, info):
    """See apple docs:
    Format is CMD=8, Code=[0-8,255], id is notification_queue.id as we provided
    1 byte, 1 byte, 4 bytes
    """
    code_desc = {0 : 'No errors encountered',
                 1 : 'Processing error',
                 2 : 'Missing device token',
                 3 : 'Missing topic',
                 4 : 'Missing payload',
                 5 : 'Invalid token size',
                 6 : 'Invalid topic size',
                 7 : 'Invalid payload size',
                 8 : 'Invalid token',
                 255 : 'Unknown'
                }
    command, code, id = struct.unpack('!BBi', response)
    if code not in code_desc:
        debugger("Response from Apple. Error code not in table (%s), APNS ID: %s" % (code, id))
    else:
        debugger("Response from Apple. Reason: %s (%s), APNS ID: %s" % (code_desc[code], code, id))
        if code == 7:
            debugger(info.get("message", ""))
