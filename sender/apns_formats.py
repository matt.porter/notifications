import struct
import time
import json
import base64

# Number of characters before the payload, needed for parsing.
PRE_PAYLOAD_LEN_SIMPLE = 37
PRE_PAYLOAD_LEN_ENHANCED = 45
# Apple's simple format. Needs a length value for payload_length
SIMPLE_FORMAT = "!BH32sH%ds"
ENHANCED_FORMAT = "!BiiH32sH%ds"


def unpack_simple_message(msg):
    """Unpacks a simple formatted message
    Returns a tuple
    Code, token_length, token, payload_length, payload
    """
    payload_length = len(msg) - PRE_PAYLOAD_LEN_SIMPLE
    fmt = SIMPLE_FORMAT % payload_length
    unpacked = struct.unpack(fmt, msg)

    return unpacked


def pack_simple_message(token, payload):
    """Packs a simple format message
    token should be base64 encoded
    payload should be apple formatted and ready to send.
    """
    token, payload = base64.b64decode(token), str(payload)
    values = (0, len(token), token, len(payload), payload)
    fmt = SIMPLE_FORMAT % len(payload)

    return struct.pack(fmt, *values)


def pack_enhanced_message(token, payload, msg_id=1, expiry=None):
    """Return a packed enhanced message.
    Expiry is set to 1 year if not provided.
    msg_id is set to 1 if not provided
    token should be base64 encoded
    payload should be apple formatted and ready to send.
    """
    token, payload = base64.b64decode(token), payload
    if expiry is None:
        expiry = long(time.time() + 365 * 86400)

    values = (1, msg_id, expiry, len(token), token, len(payload), payload)
    fmt = ENHANCED_FORMAT % len(payload)
    enhanced_message = struct.pack(fmt, *values)

    return enhanced_message


def unpack_enhanced_message(msg):
    """Unpack an enhanced formatted message.
    Returns a tuple:
    Code, msg_id, expiry, token_length, token, payload_length, payload
    """
    payload_length = len(msg) - PRE_PAYLOAD_LEN_ENHANCED
    fmt = ENHANCED_FORMAT % payload_length
    unpacked = struct.unpack(fmt, msg)

    return unpacked


def convert_simple_message_to_enhanced(id, msg, newsstand=False, expiry=None, link=False):
    """Convert an APNS message to the enhanced format by
    adding a device ID and timestamp, and prefixing with a 1 instead of a 0.
    """
    unpacked_simple = unpack_simple_message(msg)
    token = unpacked_simple[2]
    payload = unpacked_simple[4]
    try:
        payload_dict = json.loads(payload)
        if newsstand:
            payload_dict["aps"]["content-available"] = 1
            if "alert" in payload:
                del payload_dict["aps"]["alert"]
        else:
            if "content-available" in payload:
                del payload_dict["aps"]["content-available"]
        if "pmps" in payload_dict and not link:
            del payload_dict["pmps"]
        payload = json.dumps(payload_dict, separators=(',',':'))
    except Exception, e:
        print str(e), "Payload =", payload
        pass
        
    enhanced_message = pack_enhanced_message(token, payload, id, expiry)
    return enhanced_message


def build_json(message, issue, link, background):
    alert_dict = {'aps' : {}}
    if message:
        alert_dict['aps']['alert'] = message
    if link:
        alert_dict['pmps'] = {"link-url" : link}
    if background:
        alert_dict['aps']['content-available'] = 1
    if issue:
        alert_dict["new_issue_identifier"] = issue

    return json.dumps(alert_dict, separators=(',',':'))


def create_packed_message_for_db(notification_token, string_message, issue=None, link=None, msg_json=False):
    """notification token should be base64 encoded as in the DB
    message is just the text part of the message.
    if json: don't build the message
    """
    if link is None:
        linktext = ""
    else:
        linktext = ',"pmps" : {"link-url":"%s"}' % link
    if msg_json:
        msg = string_message
    else:
        msg = """{"aps":{"alert":"%s","badge":1,"sound":"default"}%s}""" % (string_message, linktext)
    t = base64.b64decode(notification_token)
    enc = pack_simple_message(t, msg)
    return enc


def test_convert_messages():
    """Test the convert_message_to_enhanced function
    Opens a file which includes an encoded message (originally taken from apns_queue)
    """
    def load_from_file(fname):
        f = open(fname, 'rb')
        buf = f.read()
        return buf
    #indexes:
    s_code = 0
    s_token_length = 1
    s_token = 2
    s_payload_length = 3
    s_payload = 4
    
    e_code = 0
    e_message_id = 1
    e_expiry = 2
    e_token_length = 3
    e_token = 4
    e_payload_length = 5
    e_payload = 6

    test_msg_id = 12345

    msg_orig = load_from_file('test_msg2.txt')
    unpacked_simple = unpack_simple_message(msg_orig)
    packed_simple = pack_simple_message(unpacked_simple[s_token], unpacked_simple[s_payload])

    packed_enhanced = convert_simple_message_to_enhanced(test_msg_id, msg_orig)
    unpacked_enhanced = unpack_enhanced_message(packed_enhanced)
    
    print "Simple:"
    print packed_simple
    print unpacked_simple
    print "Enhanced:"
    print packed_enhanced
    print unpack_enhanced_message(packed_enhanced)
    
    assert packed_simple == msg_orig
    assert len(unpacked_simple) == 5
    assert unpacked_simple[s_code] == 0
    assert unpacked_simple[s_token_length] == 32
    assert unpacked_simple[s_payload_length] == len(unpacked_simple[s_payload])
    assert unpacked_simple[s_payload] == unpacked_enhanced[e_payload]
    assert unpacked_simple[s_token] == unpacked_enhanced[e_token]
    assert unpacked_enhanced[e_expiry] > time.time()
    assert unpacked_enhanced[e_code] == 1
    assert unpacked_enhanced[e_message_id] == test_msg_id

    assert len(unpacked_enhanced) == 7


if __name__ == '__main__':
    test_convert_messages()