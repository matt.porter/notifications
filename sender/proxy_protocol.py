import json
import apns_formats
from base64 import b64encode, b64decode
import logging

class ProxyMessage(object):
	def __init__(self, bundle_name, token, payload, key=None, cert=None):
		self.bundle_name = (bundle_name)
		self.token = (token)
		self.payload = b64encode(payload)
		self.key = b64encode(key) if key else None
		self.cert = b64encode(cert) if cert else None
		logging.debug("INIT:ProxyMessage: cert = %s" % cert)

	@classmethod
	def create_from_json(cls, jsonmsg):
		"""return a Message parsed from json, or False if no message could be parsed"""
		try:
			data = json.loads(jsonmsg)
			logging.debug("Loading json OK")
		except ValueError:
			print ("Json fail: %s" % jsonmsg)
			return False

		assert 'bundle_name' in data
		assert 'token' in data
		assert 'payload' in data

		key = b64decode(data['key']) if data.get('key', False) else None
		cert = b64decode(data['cert']) if data.get('cert', False) else None

		return ProxyMessage(data['bundle_name'], data['token'], b64decode(data['payload']), 
			                key, cert)

	def serialise_to_json(self):
		return json.dumps({
			'bundle_name' : self.bundle_name,
			'token' : self.token,
			'payload' : self.payload,
			'key' : self.key,
			'cert' : self.cert
		})