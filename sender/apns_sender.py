"""APNS notification sender

Co-routine based sender of notifications
Receives payloads/notification tokens on a socket and sends them via Apple's notification service.
input should be app_bundle_name:notification_token:payload[:replacement_certificate]
output should go to the _correct_ apple socket for the app (including dev?)
sockets are kept open and managed, keeping multiple sockets open for the same app if required.

"""
import os, sys
from gevent import socket
from gevent.coros import RLock
from gevent.ssl import SSLSocket
from gevent.pywsgi import WSGIServer
import json
import base64
import tempfile

from proxy_protocol import ProxyMessage
from apns_formats import pack_enhanced_message

import logging
LOG_FILENAME = os.path.expanduser('~/apns.log')
logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)

class NoCertificateError(Exception): pass
class SocketNotCreated(Exception): pass

class CertificateStore(object):
    """The CertificateStore provides disk-backed storage
    for app certificates. This allows sockets to be reconnected 
    after a crash or shutdown.
    """
    def __init__(self, storage_path):
        self.storage_path = storage_path
        if not os.path.exists(storage_path):
            os.mkdir(storage_path)
        self.keypath = lambda bn: os.path.join(storage_path, bn+'.key')
        self.certpath = lambda bn: os.path.join(storage_path, bn+'.cert')
        self.certificates = {}

    def set_certificate(self, bundle_name, key, certificate):
        with open(self.keypath(bundle_name), 'wb') as keyf,\
             open(self.certpath(bundle_name), 'wb') as certf:
            keyf.write(key)
            certf.write(certificate)
            self.certificates[bundle_name] = self.keypath(bundle_name), self.certpath(bundle_name)
            return self.certificates[bundle_name]

    def del_certificate(self, bundle_name):
        if bundle_name in self.certificates:
            del self.certificates[bundle_name]
        for path in [self.keypath(bundle_name), self.certpath(bundle_name)]:
            if os.path.exists(path):
                os.rmmpath(self.path(bundle_name))

    def get_certificate(self, bundle_name):
        "return path to (key, certificate)"
        if bundle_name in self.certificates:
            return self.certificates[bundle_name]
        elif os.path.exists(self.keypath(bundle_name)) and os.path.exists(self.certpath(bundle_name)):
            self.certificates[bundle_name] = (self.keypath(bundle_name), self.certpath(bundle_name))
            return self.certificates[bundle_name]
        else:
            raise NoCertificateError('No certificate for %s' % bundle_name)

    def __getitem__(self, bundle_name):
        return self.get_certificate(bundle_name)

    def __setitem__(self, bundle_name, key_cert):
        key, certificate = key_cert
        return self.set_certificate(bundle_name, key, certificate)

    def __delitem__(self, bundle_name):
        return self.remove_certificate(bundle_name)


class SocketManager(object):
    """The SocketManager keeps sockets open and provides access 
    to them. Sockets are accessed on a per-app basis.
    We assume that the bundle_name is unique to get certificates.
    """
    def __init__(self, apple_url, storage_path):
        self.certificate_store = CertificateStore(storage_path)
        self.apns_service_addr = apple_url
        self.apns_service_port = 2195
        self.socket_store = {}      

    def get_socket(self, bundle_name, reconnect=False):
        if bundle_name in self.socket_store and not reconnect:
            return self.socket_store[bundle_name]
        else:
            key, certificate = self.certificate_store[bundle_name]
            sock = self.create_socket(bundle_name, key, certificate)
            if sock:
                self.socket_store[bundle_name] = (sock, RLock())
                return self.socket_store[bundle_name]
            else:
                raise SocketNotCreated()

    def create_socket(self, bundle_name, key = None, certificate = None):
        if os.path.exists(key) and os.path.exists(certificate): 
            key = open(key).read() # the key is stored, not the path to the key 
            certificate = open(certificate).read() # the certificate is stored, not the path
            self.certificate_store[bundle_name] = (key, certificate)
        keyf, certf = self.certificate_store[bundle_name]
        #sock = socket.create_connection((self.apns_service_addr, self.apns_service_port))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        logging.debug("created new socket: key %s" % keyf)
        sslsock = SSLSocket(s, keyf, certf)
        sslsock.connect((self.apns_service_addr, self.apns_service_port))
        return sslsock

    def reconnect_socket(self, bundle_name):
        return self.get_socket(bundle_name, True)


class APNSSender(object):
    def __init__(self, apple_url, storage_path):
        logging.debug('Sender created')
        if not os.path.exists(storage_path):
            os.mkdir(storage_path)
        self.socket_manager = SocketManager(apple_url, storage_path)

    def send(self, bundle_name, token, payload, key=None, certificate=None):
        if key and certificate:
            self.socket_manager.certificate_store.set_certificate(bundle_name, key, certificate)

        for attempts in range(2):
            try:
                socket, lock = self.socket_manager.get_socket(bundle_name)
                logging.debug(type(payload))
                msg = pack_enhanced_message(token, payload)
                logging.debug("Sending message %s to token %s on app %s" % (msg.decode(sys.getfilesystemencoding(), 'replace'), token, bundle_name))
                with lock:
                    socket.send(msg)
                return True
            except NoCertificateError:
                return False
            except Exception, e:
                self.socket_manager.reconnect_socket(bundle_name)
                
        return False

    def send_msg(self, msg):
        key = base64.b64decode(msg.key) if msg.key is not None else None
        cert = base64.b64decode(msg.cert) if msg.cert is not None else None
        return self.send(msg.bundle_name, msg.token, base64.b64decode(msg.payload), key, cert)

class Application(object):
    def __init__(self, storage_loc, apple_url):
        self.apns_sender = APNSSender(apple_url, storage_loc)


    def __call__(self, env, start_response):
        if env['PATH_INFO'] == '/send':
            body = env['wsgi.input']
            bodytext = body.read()
            msg = ProxyMessage.create_from_json(bodytext)
            logging.debug("Proxymsg received to send : %s" % msg.serialise_to_json())
            if not msg:
                return [json.dumps({'success' : False})]
            
            sender = self.apns_sender 
            response = sender.send_msg(msg)

            if response:
                start_response('200 OK', [('Content-Type', 'application/json')])
                return [json.dumps({'success' : True})]
            else:
                start_response('200 OK', [('Content-Type', 'application/json')])
                return [json.dumps({'success' : False, 'reason' : "Bad certificate"})]
        else:
            start_response('404 Not Found', [('Content-Type', 'text/html')])
            return ['<h1>Not Found</h1>']

if __name__ == '__main__':
    print 'Serving on 8088...'
    path = tempfile.mkdtemp(suffix='sender_storage', prefix='')
    apple_url = '127.0.0.1'
    WSGIServer(('', 8088), Application(path, apple_url)).serve_forever()