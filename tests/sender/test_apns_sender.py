import sys, os
sys.path.insert(0, os.path.abspath(__file__ + '/../../..'))

from StringIO import StringIO
import json
import tempfile
import base64
import datetime
import time
import struct

import logging
LOG_FILENAME = os.path.expanduser('~/apns.log')
logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)


import gevent
from gevent import socket
from gevent.server import StreamServer
from gevent.pywsgi import WSGIServer
from gevent import monkey; monkey.patch_socket()

from sender.apns_sender import CertificateStore
from sender.apns_sender import SocketManager
from sender.proxy_protocol import ProxyMessage

TEST_SUPPORT_DIR = os.path.abspath(__file__ + '/../../support/')
MATTS_TOKEN = "rb3Ec0Qeg6o2oinV5aBcq+Mol+7NfP+V8B1TB36KX6g="

#logging.debug("====Starting test run %s====" % datetime.datetime.now())

class TestCertificateStore(object):
    storage_folder = tempfile.mkdtemp(suffix='test_apns_certs', prefix='')
    bundle_name = 'com.pixelmags.test_app'
    key = '===TestAppCertKey==='
    cert = '===TestAppCert==='

    def test_certificate_stored_in_memory(self):
        store = CertificateStore(self.storage_folder)
        store.set_certificate(self.bundle_name, self.key, self.cert)

        assert open(store.get_certificate(self.bundle_name)[1]).read() == self.cert
        assert open(store[self.bundle_name][0]).read() == self.key

    def test_certificate_stored_on_disk(self):
        store = CertificateStore(self.storage_folder)
        
        assert open(store.get_certificate(self.bundle_name)[1]).read() == self.cert

        store = CertificateStore(self.storage_folder)

        assert open(store[self.bundle_name][0]).read() == self.key
        assert(self.bundle_name in store.certificates)


class TestSocketManager(object):
    apple_url = '127.0.0.1'
    apple_port = 2195
    storage_folder = tempfile.mkdtemp(suffix='test_apns_certs', prefix='')
    keyfile = os.path.join(TEST_SUPPORT_DIR, 'server.key')
    certfile = os.path.join(TEST_SUPPORT_DIR, 'server.crt')
    bundle_name = 'com.pixelmags.test_app'
    
    def setup_class(self):
        self.handler = StorageHandler()
        self.server = StreamServer(
            (self.apple_url, self.apple_port), self.handler.handle_apns,
            keyfile=self.keyfile, certfile=self.certfile)

        self.server.start()

    def teardown_class(self):
        self.server.stop()

    def test_sockets_open(self):
        """create a send socket to the localhost test server"""
        try:
            print "Listener started"
            SM = SocketManager(self.apple_url, self.storage_folder)
            sock = SM.create_socket(self.bundle_name, self.keyfile, self.certfile)
            sock.send('data')
            sock.close()
            print self.handler.received
        finally:
            print "end"

    def test_get_open_socket(self):
        SM = SocketManager(self.apple_url, self.storage_folder)
        sock = SM.create_socket(self.bundle_name, self.keyfile, self.certfile)
        sock, lock = SM.get_socket(self.bundle_name)
        with lock:
            sock.send('data')
        sock.close()


class TestApplication(object):
    apple_url = '127.0.0.1'
    apple_port = 2195
    storage_folder = tempfile.mkdtemp(suffix='test_apns_certs', prefix='')
    keyfile = os.path.join(TEST_SUPPORT_DIR, 'server.key')
    certfile = os.path.join(TEST_SUPPORT_DIR, 'server.crt')

    # apple_url = 'gateway.push.apple.com'
    # keyfile = os.path.join(TEST_SUPPORT_DIR, '/Users/mattporter/Documents/apns_key_mp.key')
    # certfile = os.path.join(TEST_SUPPORT_DIR, '/Users/mattporter/Documents/apns_cert_mp.crt')
    bundle_name = 'com.pixelmags.test_app'
    
    def setup_class(self):
    	# set up the test apple listener if required
        if self.apple_url == '127.0.0.1':
            self.handler = StorageHandler()
            self.server = StreamServer(
                (self.apple_url, self.apple_port), self.handler.handle_apns,
                keyfile=self.keyfile, certfile=self.certfile)
            self.server.start()

        from sender.apns_sender import Application
        loc = tempfile.mkdtemp(suffix='sender_storage', prefix='')
        self.send_server = WSGIServer(('', 8088), Application(loc, self.apple_url))
        self.send_server.start()

    def teardown_class(self):
    	if self.apple_url == '127.0.0.1':
    		self.server.stop()

        self.send_server.stop()

    def test_payload_receiver(self):
        """
        1. Set up application to receive notifications
        2. Set up listener to mock the Apple servers
        3. Send a notification with a certificate to prime the pump
        4. check that we can subsequently send a notification with no certificate
        """
        proxymsg = json.dumps({"payload" : base64.b64encode('''{"aps":{"alert":"test message","badge":1,"sound":"default"}}'''),
                                 "cert" : base64.b64encode(open(self.certfile).read()),
                                 "key" : base64.b64encode(open(self.keyfile).read()),
                                 "token" :MATTS_TOKEN,
                                 "bundle_name" : self.bundle_name
                                })
        #logging.debug("Sending proxymsg %s" % proxymsg)
        response = post_url('http://127.0.0.1:8088/send',
                             proxymsg, 'application/json')
        response = json.loads(response)
        assert('success' in response and response['success'])

        proxymsg2 = json.dumps({"payload" : base64.b64encode('''{"aps":{"alert":"test message","badge":1,"sound":"default"}}'''),
                                 "token" : MATTS_TOKEN,
                                 "bundle_name" : self.bundle_name
                                })
        response2 = post_url('http://127.0.0.1:8088/send',
                             proxymsg2, 'application/json')
        response2 = json.loads(response2)
        assert('success' in response2 and response2['success'])

    def test_multiple_apps(self):
    	proxymsg = ProxyMessage('bundle_name_1',
					    		MATTS_TOKEN,
					    		'{"aps":{"alert":"test message","badge":1,"sound":"default"}}',
					    		open(self.keyfile).read(),
					    		open(self.certfile).read()) 

        logging.debug("Sending proxymsg %s" % proxymsg.serialise_to_json())
        response = post_url('http://127.0.0.1:8088/send',
                             proxymsg.serialise_to_json(), 'application/json')
        response = json.loads(response)
        assert('success' in response and response['success'])

    	proxymsg2 = ProxyMessage('bundle_name_2',
			    		MATTS_TOKEN,
			    		'{"aps":{"alert":"test message","badge":1,"sound":"default"}}',
			    		open(self.keyfile).read(),
			    		open(self.certfile).read())

        response2 = post_url('http://127.0.0.1:8088/send',
                             proxymsg2.serialise_to_json(), 'application/json')
        response2 = json.loads(response2)
        assert('success' in response2 and response2['success'])

# @pytest.mark.skipif("0")
class TestSendSpeed(object):
    """Test how fast we can send messages"""
    apple_url = '127.0.0.1'
    apple_port = 2195
    storage_folder = tempfile.mkdtemp(suffix='test_apns_certs', prefix='')
    keyfile = os.path.join(TEST_SUPPORT_DIR, 'server.key')
    certfile = os.path.join(TEST_SUPPORT_DIR, 'server.crt')

    # apple_url = 'gateway.push.apple.com'
    # keyfile = os.path.join(TEST_SUPPORT_DIR, '/Users/mattporter/Documents/apns_key_mp.key')
    # certfile = os.path.join(TEST_SUPPORT_DIR, '/Users/mattporter/Documents/apns_cert_mp.crt')
    bundle_name = 'com.pixelmags.test_app'
    
    def setup_class(self):
        # set up the test apple listener if required
        if self.apple_url == '127.0.0.1':
            self.handler = StorageHandler()
            self.server = StreamServer(
                (self.apple_url, self.apple_port), self.handler.handle_apns,
                keyfile=self.keyfile, certfile=self.certfile)
            self.server.start()

        from sender.apns_sender import Application
        loc = tempfile.mkdtemp(suffix='sender_storage', prefix='')
        self.send_server = WSGIServer(('', 8088), Application(loc, self.apple_url))
        self.send_server.start()

    def teardown_class(self):
        if self.apple_url == '127.0.0.1':
            self.server.stop()

        self.send_server.stop()

    def test_benchmark(self):
        # first, set up, so the server has the app
        proxymsg = ProxyMessage('bundle_name_2',
                        MATTS_TOKEN,
                        '{"aps":{"alert":"test message","badge":1,"sound":"default"}}',
                        open(self.keyfile).read(),
                        open(self.certfile).read())
        #logging.debug("Sending proxymsg %s" % proxymsg)
        response = post_url('http://127.0.0.1:8088/send',
                             proxymsg.serialise_to_json(), 'application/json')
        response = json.loads(response)
        assert('success' in response and response['success'])
        
        proxymsg2 = ProxyMessage('bundle_name_2',
                        MATTS_TOKEN,
                        '{"aps":{"alert":"test message","badge":1,"sound":"default"}}',
                        )
        proxymsg = proxymsg2.serialise_to_json()
        t = time.time()
        N = 2000
        for _ in range(N):
            g = gevent.spawn(post_url, 'http://127.0.0.1:8088/send', proxymsg, 'application/json')
            g.join()
            response = g.value
            assert(response)
            response = json.loads(response)
            assert('success' in response and response['success'])

        elapsed = time.time() - t
        assert time.time() - t < 20
        logging.debug("Time to send %d messages: %s = %d messages per second" % (N, elapsed, int(N / elapsed)))


def post_url(url, data, format):
    import urllib
    import urllib2

    req = urllib2.Request(url, data)
    response = urllib2.urlopen(req)
    the_page = response.read()
    return the_page

class StorageHandler(object):
    """Store things received on this socket for tests"""
    def __init__(self):
        self.received = []
        self.state = 0
        self.format = 0
        self.last_read = None
        self.logger = logging.getLogger('AppleServerMock')
        # self.logger.debug("StorangeHandler created")

    def handle_apns(self, sock, address):
        fp = sock.makefile()
        while True:
            # if self.last_read is not None:
            #     self.logger.debug("Socket Read: %s" % self.last_read)
            # Read the simple/advanced format
            if self.state == 0:
                self.last_read = line = fp.read(1)
                self.format, = struct.unpack('!B', line)

                if int(self.format) == 0:
                    #skip steps ..
                    self.state = 3
                    self.logger.debug("Using simple format")
                elif int(self.format) == 1:
                    self.state = 1
                    self.logger.debug("Using advanced format")
                else:
                    self.state = 1 # assume advanced?
                    self.logger.debug("Unknown apns format %d" % self.format)

            # Read the identifier - advanced format only
            if self.state == 1:
                self.last_read = line = fp.read(4)
                identifier, = struct.unpack('!i', line)
                self.identifier = identifier
                self.logger.debug("Identifier is %d" % self.identifier)
                self.state = 2

            # Read the expiry date - advanced format only
            if self.state == 2:
                self.last_read = line = fp.read(4)
                expiry, = struct.unpack('!i', line)
                self.expiry = expiry
                self.logger.debug("Expiry is %d" %self.expiry)
                self.state = 3

            #Read the token length
            if self.state == 3:
                self.last_read = line = fp.read(2)
                self.token_length, = struct.unpack('!H', line)
                self.logger.debug("Token length is %d" % self.token_length)
                if self.token_length != 32:
                    self.logger.debug("token_length is normally 32 -- sure?")
                self.state = 4

            # Read the token
            if self.state == 4:
                self.last_read = line = fp.read(self.token_length)
                self.token, = struct.unpack('!%ds' % self.token_length, line)
                self.logger.debug("token is %s" % (base64.b64encode(self.token)))
                self.state = 5

            # Read payload length
            if self.state == 5:
                self.last_read = line = fp.read(2)
                self.payload_length, = struct.unpack('!H', line)
                if self.payload_length > 255:
                    self.logger.debug("Payload should NOT be greater than 255 in length, this is %d" % self.payload_length)
                self.logger.debug("Payload length %d" % self.payload_length)
                self.state = 6

            # Read payload
            if self.state == 6:
                self.last_read = line = fp.read(self.payload_length)
                self.payload, = struct.unpack('!%ds' % self.payload_length, line)
                token = base64.b64encode(self.token)
                self.logger.debug("Received message in format %d to token %s, payload %s" % (self.format, token, self.payload))
                self.received.append((token, self.payload))
                self.state = 0

            if self.state not in range(7):
                self.state = 0
                self.logger.debug("Unknown state, reverting to 0")

        sock.shutdown(socket.SHUT_WR)
        sock.close()
