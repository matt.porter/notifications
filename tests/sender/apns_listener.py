import gevent
from gevent import socket
from gevent.server import StreamServer
from gevent import monkey; monkey.patch_socket()

import tempfile
import os
import sys

import logging
logging.basicConfig(level=logging.DEBUG)

from test_apns_sender import StorageHandler


TEST_SUPPORT_DIR = os.path.abspath(__file__ + '/../../support/')
MATTS_TOKEN = "rb3Ec0Qeg6o2oinV5aBcq+Mol+7NfP+V8B1TB36KX6g="

if __name__ == "__main__":
    print "Running listener"
    apple_url = '127.0.0.1'
    apple_port = 2195
    storage_folder = tempfile.mkdtemp(suffix='test_apns_certs', prefix='')
    keyfile = os.path.join(TEST_SUPPORT_DIR, 'server.key')
    certfile = os.path.join(TEST_SUPPORT_DIR, 'server.crt')

    # apple_url = 'gateway.push.apple.com'
    # keyfile = os.path.join(TEST_SUPPORT_DIR, '/Users/mattporter/Documents/apns_key_mp.key')
    # certfile = os.path.join(TEST_SUPPORT_DIR, '/Users/mattporter/Documents/apns_cert_mp.crt')
    bundle_name = 'com.pixelmags.test_app'
    
    handler = StorageHandler()
    server = StreamServer(
        (apple_url, apple_port), handler.handle_apns,
        keyfile=keyfile, certfile=certfile)
    server.serve_forever()