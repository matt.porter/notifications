APNS Proxy Service
==================

The APNS proxy service is a simple http proxy to the Apple Push Notifications Service. It allows you to keep socket connections to Apple alive, and provides a simple HTTP interface, meaning you don't need to worry about maintaining SSL socket connections.

It handles socket and certificate caching in the server, and given a notification token and payload will build a binary message for Apple.

The responsibility for constructing the payload is on the client. If the payload fails validation then an error will be thrown.

 - [[FIXME: Define error responses]]

 - [[FIXME: Sockets should reconnect on error]]

The proxy service is written in Python using Gevent.

Tests are run using py.test in the base project directory.

Usage
=====

Run apns_sender.py with the correct apple url set.

POST JSON data to the correct IP on port 8088 with the following parameters (see send_single_apns.py for an example).

token
bundle_name
payload (base64 encoded)
key (optional, base64 encoded)
cert (optional, base64 encoded)

Suggested usage is to only send the key & certificate if the first call fails.
Response will be in JSON also, if 'success' is False, then retry including the certificate.

The certificates and keys are cached by the sending process while it is running.

Advised development procedure
=============================

Project set up
--------------
Clone the project into a local folder.
install virtualenv and virtualenvwrapper using easy_install or pip

    sudo easy_install virtualenv

Create a virtual environment for the project. This keeps installed modules separate from other projects you work on, and allows development to happen on known versions of modules.

    mkvirtualenv notifications

The command line prompt should display (notifications), signifying that you are in the correct environment. If it isn't, type:

    workon notifications

to work in the correct environment.

In this environment, we need to install the project requirements. Do

    sudo pip install -r requirements.txt


Testing
-------
Tests are run using py.test which is included in the project requirements. Tests should be written to cover all new functionality. They are located in the tests/ folder and any supporting files under the tests/support folder. This includes data or certificates that the tests rely on.

If you are developing under Sublime Text 2, then there is a build system added to the project file. This should run the tests in the project directory, but doesn't work everywhere yet.


